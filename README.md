[[_TOC_]]
# packageTool

Package tool (eg create tarballs) for installation in ATLASLocalRootBase

## Installation:

  Login to your own account
```
  cd $HOME/Tier3Dev
  git clone https://gitlab.cern.ch/atlas-tier3sw/packagetool.git 
  cd packagetool
```

## Usage:

  Use the master branch as that will contain all fixes
```
  cd $HOME/Tier3Dev/packagetool
  git pull
```

The scripts usually require an argument for the version.  These should all be
run inside a container of the same OS and arch type.

eg: it can be one of these base containers

```
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-centos7-base --noalrb
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-centos8s-base --noalrb
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-almalinux9-base --noalrb
```

If python is required, it depends on it being available in ATLASLocalRootBase in the form of eg: centos7-2.7, centos7-3.9, el9-3.9, el9-3.11 etc.  Python, in this case, is a pre-requisite.

The ALRB development containers used by this package, and created by the configurations in this repository, can be found in CERN harbor (https://registry.cern.ch/harbor/projects/3642/repositories)


### Usage (boost):
```
setupATLAS -c el9 --noalrb -r "$HOME/Tier3Dev/packagetool/make-boost.sh 1.82.0"
setupATLAS -c centos8 --noalrb -r "$HOME/Tier3Dev/packagetool/make-boost.sh 1.82.0"
```

### Usage (cppcheck):

This is meant to build on aarch64 platforms for installation on a cvmfs dir
it is run in a container _internally_
```
eg on an aarch64 platform do:
$HOME/Tier3Dev/packagetool/make-cppcheck.sh 2.11 11 7
$HOME/Tier3Dev/packagetool/make-cppcheck.sh 2.11 11 9
$HOME/Tier3Dev/packagetool/make-cppcheck.sh 2.11 13 9
```

### Usage (curl):
```
setupATLAS -c  docker://registry.cern.ch/atlas-tier3/alrbdev-centos8:latest --noalrb -r "$HOME/Tier3Dev/packagetool/make-curl.sh 8.7.1"
setupATLAS -c  docker://registry.cern.ch/atlas-tier3/alrbdev-almalinux9:latest --noalrb -r "$HOME/Tier3Dev/packagetool/make-curl.sh 8.7.1"
```

### Usage (darshan):
```
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/x86_64-centos7-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-darshan.sh 3.4.2"
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/x86_64-centos8-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-darshan.sh 3.4.2"
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-almalinux9-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-darshan.sh 3.4.2"
```

### Usage (davix):
```
setupATLAS -c  docker://registry.cern.ch/atlas-tier3/alrbdev-centos7:latest --noalrb -r "$HOME/Tier3Dev/packagetool/make-davix.sh 0.8.4"
setupATLAS -c  docker://registry.cern.ch/atlas-tier3/alrbdev-centos8:latest --noalrb -r "$HOME/Tier3Dev/packagetool/make-davix.sh 0.8.4"
setupATLAS -c  docker://registry.cern.ch/atlas-tier3/alrbdev-almalinux9:latest --noalrb -r "$HOME/Tier3Dev/packagetool/make-davix.sh 0.8.4"
```

### Usage (emi):
```
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-centos7-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-emi.sh" -m "$HOME/Tier3Dev/packagetool/repos/centos7/CentOS-Base.repo:/etc/yum.repos.d/CentOS-Base.repo"
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-centos8s-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-emi.sh"
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-almalinux9-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-emi.sh"
```

### Usage (gitlab):
```
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/x86_64-centos7-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-gitlab.sh 3.9.0"
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/x86_64-centos8-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-gitlab.sh 3.9.0"
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-almalinux9-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-gitlab.sh 3.9.0"
```

### Usage (logstash):
```
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-centos7-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-logstash.sh 2.5.0"
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-centos8s-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-logstash.sh 2.5.0"
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-almalinux9-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-logstash.sh 2.5.0"
```

### Usage (pacparser):
```
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-centos7-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-pacparser.sh v1.4.1"
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-centos8-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-pacparser.sh v1.4.1"
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-almalinux9-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-pacparser.sh v1.4.1"
```

### Usage (psutil):
```
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-centos7-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-psutil.sh 5.9.5"
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-centos8s-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-psutil.sh 5.9.5"
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-almalinux9-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-psutil.sh 5.9.5"
```

### Usage (python):
```
setupATLAS -c docker://registry.cern.ch/atlas-tier3/alrbdev-centos6:latest --noalrb -r "$HOME/Tier3Dev/packagetool/make-python.sh 2.7.18"
# will pull appropriate one for aarch64 vs x86_64
setupATLAS -c docker://registry.cern.ch/atlas-tier3/alrbdev-centos7:latest --noalrb -r "$HOME/Tier3Dev/packagetool/make-python.sh 3.8.13"
setupATLAS -c docker://registry.cern.ch/atlas-tier3/alrbdev-centos8:latest --noalrb -r "$HOME/Tier3Dev/packagetool/make-python.sh 3.8.13"
setupATLAS -c docker://registry.cern.ch/atlas-tier3/alrbdev-almalinux9:latest --noalrb -r "$HOME/Tier3Dev/packagetool/make-python.sh 3.8.13"
```

### Usage (scikit):
```
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/x86_64-alrbdev-centos7 --noalrb -r "scl enable devtoolset-7 '$HOME/Tier3Dev/packagetool/make-scikit-hep.sh v5.1.1'"
setupATLAS -c el9 --noalrb -r "$HOME/Tier3Dev/packagetool/make-scikit-hep.sh v5.1.1"
```

### Usage (stomp):
```
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-centos7-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-stomp.sh 7.0.0"
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-centos8s-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-stomp.sh 7.0.0"
setupATLAS -c /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/`uname -m`-almalinux9-base --noalrb -r "$HOME/Tier3Dev/packagetool/make-stomp.sh 7.0.0"
```

### Usage (template):
This is provided as an example of how to run a script inside a container.
Do not run inside a container; it will do it for you. eg:
```
$HOME/Tier3Dev/packagetool/templateBuild.sh 21.0--container_AthSimulation_x86_64-centos7-gcc62-opt 3.11.0
```

### Usage (varnish):
```
setupATLAS -c el9 -r "$HOME/Tier3Dev/packagetool/make-varnish.sh 7.5.0"
```

### Usage (xrootd):
```
setupATLAS -c docker://registry.cern.ch/atlas-tier3/alrbdev-centos7:latest --noalrb -r "scl enable devtoolset-7 '$HOME/Tier3Dev/packagetool/make-xrootd.sh 5.5.1'"
setupATLAS -c  docker://registry.cern.ch/atlas-tier3/alrbdev-centos8:latest --noalrb -r "$HOME/Tier3Dev/packagetool/make-xrootd.sh 5.5.1"
setupATLAS -c  docker://registry.cern.ch/atlas-tier3/alrbdev-almalinux9:latest --noalrb -r "$HOME/Tier3Dev/packagetool/make-xrootd.sh 5.5.1"
```


