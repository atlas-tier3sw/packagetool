#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! make-boost.sh
#!
#! create a tarball for boost
#!
#! Usage:
#!     make-boost.sh <version>
#!  should be run inside a container for the os/arch that you need
#!  The versions are from https://boostorg.jfrog.io/artifactory/main/release/
#!
#! History:
#!    15Feb23: A. De Silva
#!
#!----------------------------------------------------------------------------

alrb_toolName="boost"

alrb_fn_createSetupScript()
{
    
    \cat <<EOF >> $alrb_buildDir/setup.sh
if [ ! -z \$BASH_SOURCE ]; then
    alrb_scriptHome=\`\dirname \${BASH_SOURCE[0]}\`
else
    alrb_scriptHome=\`\dirname \$0\`
fi
if [ "\$alrb_scriptHome" = "." ]; then
    alrb_scriptHome=\`pwd\`
fi

export BOOST_ROOT=\$alrb_scriptHome

if [ -z "\${LD_LIBRARY_PATH}" ]; then
    export LD_LIBRARY_PATH=\$BOOST_ROOT/lib
else
    insertPath LD_LIBRARY_PATH \$BOOST_ROOT/lib
fi

return 0

EOF

    return 0
}

# main

if [ ! -z $BASH_SOURCE ]; then
    alrb_packageToolHome=`\dirname ${BASH_SOURCE[0]}`
else
    alrb_packageToolHome=`\dirname $0`
fi
if [ "$alrb_packageToolHome" = "." ]; then
    alrb_packageToolHome=`pwd`
fi

if [ "$#" -ne 1 ]; then 
    \echo "Usage: make-$alrb_toolName.sh <version>"
    \echo "       where version is a version from https://boostorg.jfrog.io/artifactory/main/release/"
    exit 64
fi
alrb_version="$1"

source $alrb_packageToolHome/setupEnvironment.sh $alrb_toolName $alrb_version
if [ $? -ne 0 ]; then
    exit 64
fi

cd $alrb_workdir

alrb_srcFile="boost_`\echo $alrb_version | \sed -e 's|\.|\_|'g`"

wget https://boostorg.jfrog.io/artifactory/main/release/$alrb_version/source/$alrb_srcFile.tar.bz2
if [ $? -ne 0 ]; then
    exit 64
fi
tar --bzip2 -xf $alrb_srcFile.tar.bz2
if [ $? -ne 0 ]; then
    exit 64
fi

alrb_srcDir="$alrb_workdir/$alrb_srcFile"
alrb_buildDir="$alrb_workdir/build"
mkdir -p $alrb_buildDir
alrb_logDir="$alrb_buildDir/logs"
mkdir -p $alrb_logDir

# build without python first ...

cd $alrb_srcDir
\echo "
./bootstrap.sh --without-libraries=python --prefix=$alrb_buildDir ...
"
./bootstrap.sh --without-libraries=python --prefix=$alrb_buildDir  2>&1 | tee $alrb_logDir/bootstrap.log
if [ $? -ne 0 ]; then
    exit 64
fi

\echo "
./b2 -a install ...
"   
./b2 install 2>&1 | tee $alrb_logDir/make-install.log
if [ $? -ne 0 ]; then
    exit 64
fi

# now build with python versions ...

for alrb_pythonVer in ${alrb_pythonAr[@]}; do
    \echo "

                   * * *
  Building $alrb_tag for python $alrb_pythonVer
                   * * *
"		   

    \rm -f $alrb_workdir/user-config.jam
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
    lsetup -f "python $alrb_osFlavor$alrb_osver-$alrb_pythonVer"
    alrb_pythonHomeDir=`readlink -f $ALRB_pythonBinDir/..`
    \echo "
using python : $alrb_pythonVer : $ALRB_pythonBinDir/python$alrb_pythonVer : $alrb_pythonHomeDir/include/python$alrb_pythonVer : $alrb_pythonHomeDir/lib;
" > $alrb_workdir/user-config.jam 	

    \echo "
$alrb_workdir/user-config.jam is:
"
    \cat $alrb_workdir/user-config.jam
    
    export HOME=$alrb_workdir

    cd $alrb_srcDir
    \echo "
./bootstrap.sh --with-libraries=python --with-python=python$alrb_pythonVer --prefix=$alrb_buildDir ...
"
    ./bootstrap.sh --with-libraries=python --with-python=python$alrb_pythonVer --prefix=$alrb_buildDir  2>&1 | tee $alrb_logDir/bootstrap-$alrb_pythonVer.log
    if [ $? -ne 0 ]; then
	exit 64
    fi

    \echo "
    ./b2 -a install ...
"   
    ./b2 install 2>&1 | tee $alrb_logDir/make-install-$alrb_pythonVer.log
    if [ $? -ne 0 ]; then
	exit 64
    fi

done
    
alrb_fn_createSetupScript

cd $alrb_buildDir
gtar zcf $alrb_workdir/$alrb_tag.tgz *
\echo "Ready: $alrb_workdir/$alrb_tag.tgz"

exit 0

