#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! make-cppcheck.sh
#!
#! create a tarball for cppcheck
#!
#! Usage:
#!     make-cppcheck.sh <cppcheck version> <gcc version> <OS version>
#!  This should NOT be run inside a container; it will be build inside
#!   a container matching the "OS version" arg above
#!
#! History:
#!    18Aug23: A. De Silva
#!
#!----------------------------------------------------------------------------

alrb_tool="cppcheck"

if [ "$#" -ne 3 ]; then 
    \echo "Usage: make-$alrb_tool.sh <$alrb_tool version> <gcc version> <os version>"
    \echo "       where version is a version from https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/"
    \echo "             gcc version is the major version of gcc - eg 11 or 13"
    \echo "             os version is eg 7 or 9 for centos7 or el9"
    exit 64
fi
alrb_version="$1"
alrb_gccTag="$2"
alrb_osver="$3"

if [ $alrb_osver -ge 9 ]; then
    alrb_osFlavor="el"
else
    alrb_osFlavor="centos"
fi

alrb_unameM=`uname -m`
if [ $alrb_unameM = "aarch64" ]; then
    alrb_archDir="aarch64-Linux"
else
    alrb_archDir="x86_64"
fi

# version tag should be 1st 2 gigits
alrb_version2D=`\echo $alrb_version | \cut -f -2 -d "."`
alrb_tag="$alrb_version2D-$alrb_unameM-$alrb_osFlavor$alrb_osver-gcc$alrb_gccTag"

if [ -z $TMPDIR ]; then
    \echo "TMPDIR not defined.  Will use PWD=$PWD"
    alrb_workdir=$PWD/$alrb_tool/$alrb_tag
else
    alrb_workdir=$TMPDIR/$alrb_tool/$alrb_tag
fi
\rm -rf $alrb_workdir
alrb_mockcvmfs=$alrb_workdir/mockCvmfs
alrb_hostInstallDir=$alrb_mockcvmfs/$alrb_tag
mkdir -p $alrb_hostInstallDir
cd $alrb_hostInstallDir

\rm -f $alrb_version.tar.gz
wget https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/$alrb_version.tar.gz
if [ $? -ne 0 ]; then
    exit 64
fi
tar zxf $alrb_version.tar.gz
if [ $? -ne 0 ]; then
    exit 64
fi
\rm $alrb_version.tar.gz

alrb_InstallDir=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/$alrb_archDir/cppcheck/$alrb_tag

cd $alrb_workdir

\cat <<EOF >> $alrb_hostInstallDir/setup.sh
if [ ! -z \$BASH_SOURCE ]; then
    alrb_scriptHome=\`\dirname \${BASH_SOURCE[0]}\`
else
    alrb_scriptHome=\`\dirname \$0\`
fi
if [ "\$alrb_scriptHome" = "." ]; then
    alrb_scriptHome=\`pwd\`
fi     
EOF

\cat << EOF > $alrb_workdir/build.sh 

cd $alrb_InstallDir

alrb_tmpVal=\`\find $alrb_InstallDir -maxdepth 2 -type f -name Makefile\`
if [ \$? -ne 0 ]; then
    \echo "Error: Makefile is missing in source code after unpacking ?"
    exit 64
fi
alrb_tmpVal=\`readlink -f \$alrb_tmpVal\`

alrb_cppInstallDir=\`\dirname \$alrb_tmpVal\`
alrb_cppSrcDir="$alrb_InstallDir/src"
alrb_cppLogDir="$alrb_InstallDir/logs"
\mkdir -p \$alrb_cppLogDir
\mv \$alrb_cppInstallDir \$alrb_cppSrcDir
\mkdir -p \$alrb_cppInstallDir
cd \$alrb_cppSrcDir

# temporary !  us asetup 02-02-05 or newer
#export AtlasSetup="\$ATLAS_LOCAL_ROOT/AtlasSetup/testing/AtlasSetup"
source \$AtlasSetup/scripts/asetup.sh none,gcc$alrb_gccTag,cmakesetup

\echo " * * * * * "
gcc --version; cmake --version; cmake3 --version
\echo " * * * * * "

\mkdir -p \$alrb_cppSrcDir/build
cd \$alrb_cppSrcDir/build

cmake -DCMAKE_INSTALL_PREFIX:PATH=\$alrb_cppInstallDir/ -DCMAKE_BUILD_TYPE:STRING=Release -DUSE_MATCHCOMPILER:BOOL=ON \$alrb_cppSrcDir  2>&1 | tee \$alrb_cppLogDir/cmake.log
if [ \$? -ne 0 ]; then
    exit 64
fi

make 2>&1 | tee \$alrb_cppLogDir/make.log
if [ \$? -ne 0 ]; then
    exit 64
fi

make install 2>&1 | tee \$alrb_cppLogDir/make-install.log
if [ \$? -ne 0 ]; then
    exit 64
fi

alrb_tmpVal=\`basename \$alrb_cppInstallDir\`

\echo "
export CPPCHECK_SRC="\\\$alrb_scriptHome/src"
export CPPCHECK_HOME="\\\$alrb_scriptHome/\$alrb_tmpVal"
insertPath PATH \\\$CPPCHECK_HOME/bin        

unset alrb_scriptHome
" >> $alrb_InstallDir/setup.sh


EOF


if [ -z $ATLAS_LOCAL_ROOT_BASE ]; then
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
fi
#export APPTAINERENV_ATLAS_LOCAL_ROOT_BASE=/alrb/ATLASLocalRootBase
#source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -c $alrb_osFlavor$alrb_osver --nocvmfs -e "-B $alrb_mockcvmfs:/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/$alrb_archDir/cppcheck/ -B /cvmfs/sft.cern.ch -B /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase:/alrb/ATLASLocalRootBase" -r "source /srv/build.sh"
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -c $alrb_osFlavor$alrb_osver -e "-B $alrb_mockcvmfs:/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/$alrb_archDir/cppcheck/" -r "source /srv/build.sh"
if [ $? -ne 0 ]; then
    exit 64
fi

cd $alrb_hostInstallDir
gtar zcf $alrb_workdir/$alrb_tag.tgz *
if [ $? -eq 0 ]; then
    \echo "Ready: $alrb_workdir/$alrb_tag.tgz"
else
    exit 64
fi

exit 0
