#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! make-curl.sh
#!
#! create a tarball for curl
#!
#! Usage:
#!     make-curl.sh <version>
#!  should be run inside a container for the os/arch that you need
#!  The versions are from https://curl.se/download
#!
#! History:
#!    03Apr24: A. De Silva
#!
#!----------------------------------------------------------------------------

alrb_toolName="curl"

alrb_fn_createSetupScript()
{
    
    \cat <<EOF >> $alrb_buildDir/setup.sh
if [ ! -z \$BASH_SOURCE ]; then
    alrb_scriptHome=\`\dirname \${BASH_SOURCE[0]}\`
else
    alrb_scriptHome=\`\dirname \$0\`
fi
if [ "\$alrb_scriptHome" = "." ]; then
    alrb_scriptHome=\`pwd\`
fi

if [ -z "\${LD_LIBRARY_PATH}" ]; then
    export LD_LIBRARY_PATH=\$alrb_scriptHome/lib
else
    insertPath LD_LIBRARY_PATH \$alrb_scriptHome/lib
fi

insertPath PATH \$alrb_scriptHome/bin    	

return 0

EOF

    return 0
}

# main

if [ ! -z $BASH_SOURCE ]; then
    alrb_packageToolHome=`\dirname ${BASH_SOURCE[0]}`
else
    alrb_packageToolHome=`\dirname $0`
fi
if [ "$alrb_packageToolHome" = "." ]; then
    alrb_packageToolHome=`pwd`
fi

if [ "$#" -ne 1 ]; then 
    \echo "Usage: make-$alrb_toolName.sh <version>"
    \echo "       where version is a version from https://curl.se/download/"
    exit 64
fi
alrb_version="$1"

source $alrb_packageToolHome/setupEnvironment.sh $alrb_toolName $alrb_version
if [ $? -ne 0 ]; then
    exit 64
fi

cd $alrb_workdir

alrb_srcFile="curl-$alrb_version"

wget https://curl.se/download/$alrb_srcFile.tar.gz
if [ $? -ne 0 ]; then
    exit 64
fi
tar zxf $alrb_srcFile.tar.gz
if [ $? -ne 0 ]; then
    exit 64
fi

alrb_srcDir="$alrb_workdir/$alrb_srcFile"
alrb_buildDir="$alrb_workdir/build"
mkdir -p $alrb_buildDir
alrb_logDir="$alrb_buildDir/logs"
mkdir -p $alrb_logDir

cd $alrb_srcDir
\echo "
./configure --with-gssapi --with-openssl --prefix=$alrb_buildDir ...
"
./configure --with-gssapi --with-openssl --prefix=$alrb_buildDir 2>&1 | tee $alrb_logDir/configure.log
if [ $? -ne 0 ]; then
    exit 64
fi

\echo "
make ...
"   
make 2>&1 | tee $alrb_logDir/make.log
if [ $? -ne 0 ]; then
    exit 64
fi

\echo "
make test ...
"   
make test 2>&1 | tee $alrb_logDir/make-test.log
if [ $? -ne 0 ]; then
    exit 64
fi

\echo "
make install ...
"   
make install 2>&1 | tee $alrb_logDir/make-install.log
if [ $? -ne 0 ]; then
    exit 64
fi
    
alrb_fn_createSetupScript

cd $alrb_buildDir
gtar zcf $alrb_workdir/$alrb_tag.tgz *
\echo "Ready: $alrb_workdir/$alrb_tag.tgz"

exit 0

