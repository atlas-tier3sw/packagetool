#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! make-darshan.sh
#!
#! create a tarball for darshan
#!
#! Usage:
#!     make-darshan.sh <version>
#!  should be run inside a container for the os/arch that you need
#!  The versions are from https://www.mcs.anl.gov/research/projects/darshan/
#!
#! History:
#!    15Feb23: A. De Silva
#!
#!----------------------------------------------------------------------------

alrb_toolName="darshan"

alrb_fn_createSetupInstallScript()
{
    \cat <<EOF > $alrb_installDir/install.sh
export DARSHAN_INSTALLEDDIR="\`dirname \${BASH_SOURCE:-\$0}\`"

darshan_tmpAr=(`cd $alrb_installDir; \find . -type f -exec grep -Iq -e "$alrb_installDir"  {} \; -print`)
for darshan_tmp in \${darshan_tmpAr[@]}; do
    \sed -e 's|'$alrb_installDir'|'\$DARSHAN_INSTALLEDDIR'|g' \$darshan_tmp > \$darshan_tmp.new
    if [ \$? -eq 0 ]; then
      chmod --reference \$darshan_tmp \$darshan_tmp.new	
      \mv \$darshan_tmp.new  \$darshan_tmp
    fi  
done      

unset darshan_tmpAr darshan_tmp
EOF
    return 0
}


alrb_fn_createSetupScript()
{
    \cat <<EOF > $alrb_installDir/setup.sh

export DARSHAN_INSTALLEDDIR="\`dirname \${BASH_SOURCE:-\$0}\`"

export PATH="\$PATH:\$DARSHAN_INSTALLEDDIR/bin"

if [ -z \$LD_LIBRARY_PATH ]; then
  export LD_LIBRARY_PATH="\$DARSHAN_INSTALLEDDIR/lib"
else
  export LD_LIBRARY_PATH="\$LD_LIBRARY_PATH:\$DARSHAN_INSTALLEDDIR/lib"
fi

if [ -z \$CPLUS_INCLUDE_PATH ]; then
  export CPLUS_INCLUDE_PATH="\$DARSHAN_INSTALLEDDIR/include"
else
  export CPLUS_INCLUDE_PATH="\$CPLUS_INCLUDE_PATH:\$DARSHAN_INSTALLEDDIR/include"
fi

if [ -z \$C_INCLUDE_PATH ]; then
  export C_INCLUDE_PATH="\$DARSHAN_INSTALLEDDIR/include"
else
  export C_INCLUDE_PATH="\$C_INCLUDE_PATH:\$DARSHAN_INSTALLEDDIR/include"
fi

if [ -z \$DARSHAN_LOGDIR ]; then
   export DARSHAN_LOGDIR=\`pwd\`
fi

if [ -z \$DARSHAN_LD_PRELOAD ]; then
    export DARSHAN_LD_PRELOAD="\$DARSHAN_INSTALLEDDIR/lib/libdarshan.so"
fi

if [ -z \$DARSHAN_ENABLE_NONMPI ]; then
    export DARSHAN_ENABLE_NONMPI=1
fi

EOF
    return 0
}

# main

if [ ! -z $BASH_SOURCE ]; then
    alrb_packageToolHome=`\dirname ${BASH_SOURCE[0]}`
else
    alrb_packageToolHome=`\dirname $0`
fi
if [ "$alrb_packageToolHome" = "." ]; then
    alrb_packageToolHome=`pwd`
fi

if [ "$#" -ne 1 ]; then 
    \echo "Usage: make-$alrb_toolName.sh <version>"
    \echo "       where version is a version from https://ftp.mcs.anl.gov/pub/darshan/releases/"
    exit 64
fi
alrb_version="$1"

source $alrb_packageToolHome/setupEnvironment.sh $alrb_toolName $alrb_version
if [ $? -ne 0 ]; then
    exit 64
fi

cd $alrb_workdir

curl -O https://ftp.mcs.anl.gov/pub/darshan/releases/darshan-$alrb_version.tar.gz
if [ $? -ne 0 ]; then
    exit 64
fi
tar zxf darshan-$alrb_version.tar.gz
if [ $? -ne 0 ]; then
    exit 64
fi
alrb_darshanSrc="$alrb_workdir/darshan-$alrb_version"
cd $alrb_darshanSrc
alrb_logDir="$alrb_darshanSrc/logDir"
mkdir -p $alrb_logDir

. ./prepare.sh 2>&1 | tee $alrb_logDir/prepare.log
if [ $? -ne 0 ]; then
    exit 64
fi

alrb_installDir="$alrb_darshanSrc//myBuild"
\mkdir -p $alrb_installDir

./configure --with-log-path-by-env=DARSHAN_LOGDIR​ --with-jobid-env=NONE \
--disable-silent-rules --enable-group-readable-logs --disable-lustre-mod \
--without-mpi --prefix=$alrb_installDir 2>&1 | tee $alrb_logDir/configure.log
if [ $? -ne 0 ]; then
    exit 64
fi

make 2>&1 | tee $alrb_logDir/make.log
if [ $? -ne 0 ]; then
    exit 64
fi

make install 2>&1 | tee $alrb_logDir/make-install.log
if [ $? -ne 0 ]; then
    exit 64
fi

alrb_fn_createSetupScript
alrb_fn_createSetupInstallScript

cd $alrb_installDir
cp -r $alrb_logDir ./
gtar zcf $alrb_workdir/$alrb_tag.tar.gz *
\echo "Ready: $alrb_workdir/$alrb_tag.tar.gz"

exit 0
