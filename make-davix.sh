#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! make-davix.sh
#!
#! create a tarball for davix
#!
#! Usage:
#!     make-davix.sh <version>
#!  should be run inside a container for the os/arch that you need
#!  The version is a tag from https://github.com/cern-fts/davix
#!
#! History:
#!    07Mar22: A. De Silva
#!
#!----------------------------------------------------------------------------

alrb_toolName="davix"

if [ ! -z $BASH_SOURCE ]; then
    alrb_packageToolHome=`\dirname ${BASH_SOURCE[0]}`
else
    alrb_packageToolHome=`\dirname $0`
fi
if [ "$alrb_packageToolHome" = "." ]; then
    alrb_packageToolHome=`pwd`
fi

if [ "$#" -ne 1 ]; then 
    \echo "Usage: make-$alrb_toolName.sh <version>"
    \echo "       where version is a tag from https://github.com/cern-fts/davix" 
    exit 64
fi
alrb_version="$1"

source $alrb_packageToolHome/setupEnvironment.sh $alrb_toolName $alrb_version
if [ $? -ne 0 ]; then
    exit 64
fi

cd $alrb_workdir

alrb_versionTag="R_`\echo $alrb_version | \sed -e 's|\.|_|g'`"
git clone https://github.com/cern-fts/davix.git davix
if [ $? -ne 0 ]; then
    exit 64
fi
cd davix
cp packaging/make-binary-dist.sh $alrb_workdir/
git checkout tags/$alrb_versionTag
if [ $? -ne 0 ]; then
    exit 64
fi
if [ ! -e packaging/make-binary-dist.sh ]; then
    cp $alrb_workdir/make-binary-dist.sh packaging/make-binary-dist.sh
fi
\sed -e 's|cmake3 |cmake |' packaging/make-binary-dist.sh > packaging/make-binary-dist-v1.sh
chmod +x packaging/make-binary-dist-v1.sh

if [ -z $ATLAS_LOCAL_ROOT_BASE ]; then
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
fi
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
lsetup cmake

./packaging/make-binary-dist-v1.sh --skip-dependencies 2>&1 | tee $alrb_workdir/make-binary-dist.log
if [ $? -ne 0 ]; then
    exit 64
fi

cd $alrb_workdir/davix/binary-tarball
mkdir -p $alrb_workdir/davix/binary-tarball/buildLogs
\cp $alrb_workdir/make-binary-dist.log $alrb_workdir/davix/binary-tarball/buildLogs/

gtar zcf $alrb_workdir/$alrb_tag.tar.gz *
\echo "Ready: $alrb_workdir/$alrb_tag.tar.gz"

exit 0
