#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! make-emi.sh
#!
#! create a tarball for grid middleware UI on the same platform/os
#!
#! Usage:
#!     make-emi.sh
#!  should be run inside a container for the os/arch that you need.
#!  version created will be a timestamp snapshot eg: 20220307-snapshot
#!
#! History:
#!    07Mar22: A. De Silva, First version based on Matt Doidge's recipe
#!
#!----------------------------------------------------------------------------

alrb_toolName="emi"

alrb_fn_buildPythonBindings()
{
    local alrb_emiContainer=""
    if [ $alrb_osver -le 7 ]; then
	return 0
    elif [ $alrb_osver -eq 8 ]; then
	alrb_emiContainer="docker://registry.cern.ch/atlas-tier3/emi-centos8:latest"
    elif [ $alrb_osver -eq 9 ]; then
	alrb_emiContainer="docker://registry.cern.ch/atlas-tier3/emi-el9:latest"
    else
	\echo "Error: container not found for OS $alrb_osver"
	exit 64
    fi

    \echo "Building python bindings ..."
    
    cd $alrb_workdir
    \rm -rf gfal2
    mkdir gfal2
    cd gfal2
    export HOME=`pwd`
    cd $HOME

    \echo "git clone --branch v$alrb_gfalVer https://github.com/cern-fts/gfal2-python.git ..." 
    git clone --branch v$alrb_gfalVer https://github.com/cern-fts/gfal2-python.git
    if [ $? -ne 0 ]; then
	exit 64
    fi    
    
    for alrb_pythonVer in ${alrb_pythonAr[@]}; do

	\echo "	    
	           * * *
  Building $alrb_tag pythonbindings for python $alrb_pythonVer
                   * * *
"		   

	local alrb_skipBinding=""
	local alrb_tmpDir="$alrb_srcHome/usr/lib64/python$alrb_pythonVer"
	if [[ -d $alrb_tmpDir ]] && [[ ! -L $alrb_tmpDir ]]; then
	    alrb_skipBinding="YES"
	elif [ -L $alrb_tmpDir ]; then
	    \rm -f $alrb_tmpDir
	fi
	mkdir -p $alrb_tmpDir
	
	alrb_pyALRBVersion="$alrb_osFlavor$alrb_osver-$alrb_pythonVer"

	alrb_pythonVerNoDot="`\echo $alrb_pythonVer | \sed -e 's|\.||g'`"
	alrb_pythonVerDot="`\echo $alrb_pythonVer | \sed -e 's|\.|\\.|g'`"
	
	local alrb_payload="$HOME/payload-${alrb_pyALRBVersion}.sh"
	\rm -rf $alrb_payload
	\cat <<EOF > $alrb_payload
#!/bin/bash
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
lsetup -f boost "python $alrb_pyALRBVersion"

mkdir -p \$HOME/usr/lib64/
\cp -a \$BOOST_ROOT/lib/libboost_python${alrb_pythonVerNoDot}.so* \$HOME/usr/lib64/

export C_INCLUDE_PATH=\$BOOST_ROOT/include
export CPLUS_INCLUDE_PATH=\$BOOST_ROOT/include

cd \$HOME/gfal2-python
alrb_cmakeBuildDir="\$HOME/gfal2-python/cmake-build-py$alrb_pythonVerDot"
\rm -rf \$alrb_cmakeBuildDir
mkdir \$alrb_cmakeBuildDir
cd \$alrb_cmakeBuildDir

\echo "Cmake3 version: \`cmake3 --version\`"

\echo " cmake3 ../ -DBOOST_ROOT=\$BOOST_ROOT -DPython_EXECUTABLE=\$ALRB_pythonBinDir/python3  -Wno-dev --fresh ..."
cmake3 ../ -DBOOST_ROOT=\$BOOST_ROOT -DPython_EXECUTABLE=\$ALRB_pythonBinDir/python3  -Wno-dev --fresh
if [ \$? -ne 0 ]; then
    exit 64
fi

if [ "$alrb_skipBinding" != "YES" ]; then	
  \echo "make DESTDIR=\$HOME/build-gfal2 install ..."
  make DESTDIR=\$HOME/build-gfal2 install	    
  if [ \$? -ne 0 ]; then
      exit 64
  fi
fi

exit 0
EOF
	chmod +x $alrb_payload
	\echo " * * * python binding payload file * * *"
	\cat $alrb_payload
	\echo "           * * * * * *"
	
	if [ -z $ATLAS_LOCAL_ROOT_BASE ]; then
  	    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
	fi
	if [[ -z $SINGULARITY_TMPDIR ]] || [[ -z $APPTAINER_TMPDIR ]]; then
	    export APPTAINER_TMPDIR=$TMPDIR/apptainer-tmp
	    mkdir -p $APPTAINER_TMPDIR
	fi	    
	source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c $alrb_emiContainer -r "source $alrb_payload" --shell bash

	if [ $? -eq 0 ]; then
	    if [ "$alrb_skipBinding" != "YES" ]; then
		alrb_tmpDir=`\find $HOME/build-gfal2 -type d -name python$alrb_pythonVer`
		if [ $? -eq 0 ]; then
		    \cp -aR $alrb_tmpDir $alrb_srcHome/usr/lib64/
		else
		    \echo "Error: gfal.so lib not found in $HOME/build-gfal2"
		    return 64
		fi
		\cp -a $HOME/usr/lib64/libboost_python${alrb_pythonVerNoDot}.so* $alrb_srcHome/usr/lib64/
	    else
		 \echo " Skipping installing python$alrb_pythonVer because it is already there !"
	    fi
	else
	    return 64
	fi
    done
        
    return 0
}


alrb_fn_buildPythonBindingsPre113()
{
    local alrb_emiContainer=""
    if [ $alrb_osver -le 7 ]; then
	return 0
    elif [ $alrb_osver -eq 8 ]; then
	alrb_emiContainer="docker://registry.cern.ch/atlas-tier3/emi-centos8:latest"
    elif [ $alrb_osver -eq 9 ]; then
	alrb_emiContainer="docker://registry.cern.ch/atlas-tier3/emi-el9:latest"
    else
	\echo "Error: container not found for OS $alrb_osver"
	exit 64
    fi

    \echo "Building python bindings ..."
    
    cd $alrb_workdir
    \rm -rf gfal2
    mkdir gfal2
    cd gfal2
    export HOME=`pwd`
    cd $HOME

    alrb_sedStr=""
    alrb_grepStr=""
    for alrb_pythonVer in ${alrb_pythonAr[@]}; do
	alrb_pythonVerNoDot="`\echo $alrb_pythonVer | \sed -e 's|\.||g'`"
	alrb_pythonVerDot="`\echo $alrb_pythonVer | \sed -e 's|\.|\\.|g'`"
	alrb_grepStr="$alrb_grepStr -e 'python$alrb_pythonVerNoDot' -e 'python$alrb_pythonVerDot'"
	alrb_sedStr="$alrb_sedStr -e 's|python$alrb_pythonVerNoDot|ALRB_FIX_PYVER_ND|g' -e 's|python$alrb_pythonVerDot|ALRB_FIX_PYVER_D|g'"
    done

    \echo "git clone --branch v$alrb_gfalVer https://github.com/cern-fts/gfal2-python.git ..." 
    git clone --branch v$alrb_gfalVer https://github.com/cern-fts/gfal2-python.git
    if [ $? -ne 0 ]; then
	exit 64
    fi    

    for alrb_pythonVer in ${alrb_pythonAr[@]}; do

	\echo "	    
	           * * *
  Building $alrb_tag pythonbindings for python $alrb_pythonVer
                   * * *
"		   

	local alrb_skipBinding=""
	local alrb_tmpDir="$alrb_srcHome/usr/lib64/python$alrb_pythonVer"
	if [[ -d $alrb_tmpDir ]] && [[ ! -L $alrb_tmpDir ]]; then
	    alrb_skipBinding="YES"
	elif [ -L $alrb_tmpDir ]; then
	    \rm -f $alrb_tmpDir
	fi
	mkdir -p $alrb_tmpDir
	
	alrb_pyALRBVersion="$alrb_osFlavor$alrb_osver-$alrb_pythonVer"

	alrb_pythonVerNoDot="`\echo $alrb_pythonVer | \sed -e 's|\.||g'`"
	alrb_pythonVerDot="`\echo $alrb_pythonVer | \sed -e 's|\.|\\.|g'`"
	alrb_grepStr0="-e 'ALRB_FIX_PYVER_D' -e 'ALRB_FIX_PYVER_ND'"
	alrb_sedStr0="-e 's|ALRB_FIX_PYVER_D|python$alrb_pythonVerDot|g' -e 's|ALRB_FIX_PYVER_ND|python$alrb_pythonVerNoDot|g'"
	
	local alrb_payload="$HOME/payload-${alrb_pyALRBVersion}.sh"
	\rm -rf $alrb_payload
	\cat <<EOF > $alrb_payload
#!/bin/bash
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
lsetup -f boost "python $alrb_pyALRBVersion"

mkdir -p \$HOME/usr/lib64/
\cp -a \$BOOST_ROOT/lib/libboost_python${alrb_pythonVerNoDot}.so* \$HOME/usr/lib64/

export C_INCLUDE_PATH=\$BOOST_ROOT/include
export CPLUS_INCLUDE_PATH=\$BOOST_ROOT/include

if [ ! -e \$HOME/cmake-build.tgz ]; then
  cd \$HOME/gfal2-python
  \rm -rf cmake-build
  mkdir cmake-build/
  cd cmake-build/

  \echo " cmake3 ../ -DBoost_LIBRARYDIR=\$BOOST_ROOT/lib  -Wno-dev ..."
  cmake3 ../ -DBoost_LIBRARYDIR=\$BOOST_ROOT/lib  -Wno-dev
  if [ \$? -ne 0 ]; then
      exit 64
  fi

  \echo "Temporary patching for a global var ..."
  for alrb_item in \`\grep -r $alrb_grepStr src/* | \cut -f 1 -d ":" | \sort -u\`; do
    \echo "patch \$alrb_item ..."
    \sed $alrb_sedStr \$alrb_item > \$alrb_item.new
    if [ \$? -eq 0 ]; then
      chmod --reference \$alrb_item \$alrb_item.new
      mv \$alrb_item.new \$alrb_item
    else
      \echo "Error: \$alrb_item could not be patched for python"
    fi
  done

  gtar zcf \$HOME/cmake-build.tgz *
fi


cd \$HOME
rm -rf  \$HOME/gfal2-python/cmake-build
mkdir -p \$HOME/gfal2-python/cmake-build/
cd \$HOME/gfal2-python/cmake-build/
gtar zxf \$HOME/cmake-build.tgz

\echo "Fixing python version in src dir ..."
for alrb_item in \`\grep -r $alrb_grepStr0 src/* | \cut -f 1 -d ":" | \sort -u\`; do
  \echo "fixing \$alrb_item ..."
  \sed $alrb_sedStr0 \$alrb_item > \$alrb_item.new
  if [ \$? -eq 0 ]; then
    chmod --reference \$alrb_item \$alrb_item.new
    mv \$alrb_item \$alrb_item.old
    mv \$alrb_item.new \$alrb_item
  else
    \echo "Error: \$alrb_item could not be fixed for python"
  fi
done

if [ "$alrb_skipBinding" != "YES" ]; then	
  \echo "make DESTDIR=\$HOME/build-gfal2 install ..."
  make DESTDIR=\$HOME/build-gfal2 install	    
  if [ \$? -ne 0 ]; then
      exit 64
  fi
fi

exit 0
EOF
	chmod +x $alrb_payload
	\echo " * * * python binding payload file * * *"
	\cat $alrb_payload
	\echo "           * * * * * *"
	
	if [ -z $ATLAS_LOCAL_ROOT_BASE ]; then
  	    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
	fi
	if [[ -z $SINGULARITY_TMPDIR ]] || [[ -z $APPTAINER_TMPDIR ]]; then
	    export APPTAINER_TMPDIR=$TMPDIR/apptainer-tmp
	    mkdir -p $APPTAINER_TMPDIR
	fi	    
	source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c $alrb_emiContainer -r "source $alrb_payload" --shell bash

	if [ $? -eq 0 ]; then
	    if [ "$alrb_skipBinding" != "YES" ]; then
		\cp -aR $HOME/build-gfal2/usr/lib64/python$alrb_pythonVer $alrb_srcHome/usr/lib64/		
		\cp -a $HOME/usr/lib64/libboost_python${alrb_pythonVerNoDot}.so* $alrb_srcHome/usr/lib64/
	    else
		 \echo " Skipping installing python$alrb_pythonVer because it is already there !"
	    fi
	else
	    return 64
	fi
    done
        
    return 0
}


alrb_fn_createPriorityBinDir()
{

    cd $alrb_srcHome/usr/bin
    \mkdir -p pathPriority
    cd pathPriority
    local alrb_tmpAr=(
	gfal* 
	arc* 
	voms* 
	grid* 
	myproxy*
    )
    local alrb_pattern
    local alrb_item
    for alrb_pattern in ${alrb_tmpAr[@]}; do
	for alrb_item in `\find ../ -name $alrb_pattern`; do
	    ln -s $alrb_item
	done
    done

    return 0
}


alrb_fn_createSetupScript()
{
    \cat <<EOF > ./setup.sh
    
export EMI_TARBALL_BASE=fixMe_EMI_TARBALL_BASE

export PATH=\$EMI_TARBALL_BASE/usr/bin/pathPriority:\$PATH:\$EMI_TARBALL_BASE/usr/bin:\$EMI_TARBALL_BASE/usr/sbin

# very important to have 64-bit libs first in path
export LD_LIBRARY_PATH=\$EMI_TARBALL_BASE/usr/lib:\$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=\$EMI_TARBALL_BASE/usr/lib64:\$LD_LIBRARY_PATH

export GLITE_LOCATION=\$EMI_TARBALL_BASE/usr
export GLITE_LOCATION_VAR=\$EMI_TARBALL_BASE/var

export LCG_LOCATION=\$EMI_TARBALL_BASE/usr
if [ -d \$ATLAS_LOCAL_ROOT_BASE/etc/vomses ]; then
  export VOMS_USERCONF=\$ATLAS_LOCAL_ROOT_BASE/etc/vomses
else
  export VOMS_USERCONF=\$EMI_TARBALL_BASE/etc/vomses
fi
export SRM_PATH=\$EMI_TARBALL_BASE/usr/share/srm
export GLOBUS_LOCATION=\$EMI_TARBALL_BASE/usr
export BDII_LIST=lcg-bdii.egi.eu:2170
if [ -d \$ATLAS_LOCAL_ROOT_BASE/etc/vomses ]; then
  export X509_VOMSES=\$ATLAS_LOCAL_ROOT_BASE/etc/vomses
else
  export X509_VOMSES=\$EMI_TARBALL_BASE/etc/vomses
fi
if [ -d \$ATLAS_LOCAL_ROOT_BASE/etc/grid-security-emi/certificates ]; then
  export X509_CERT_DIR=\$ATLAS_LOCAL_ROOT_BASE/etc/grid-security-emi/certificates
else
  export X509_CERT_DIR=\$EMI_TARBALL_BASE/etc/grid-security/certificates
fi

if [ -d \$ATLAS_LOCAL_ROOT_BASE/etc/grid-security-emi/vomsdir ]; then
   export X509_VOMS_DIR=\$ATLAS_LOCAL_ROOT_BASE/etc/grid-security-emi/vomsdir
else
  export X509_VOMS_DIR=\$EMI_TARBALL_BASE/etc/grid-security/vomsdir
fi

# allow for switching between python and python3
if [ -z \$EMI_PYTHONBIN ]; then
    export EMI_PYTHONBIN="python"
fi
which \$EMI_PYTHONBIN >/dev/null 2>&1
if [ \$? -ne 0 ]; then
    if [ "\$EMI_PYTHONBIN" = "python3" ]; then
      # python3 not found but python points to python 3.X so use it !
      emi_tmpVal=\`python -V 2>&1 | \awk '{print \$2}' | \cut -d "." -f 1\`
      if [ "\$emi_tmpVal" = "3" ]; then
          export EMI_PYTHONBIN="python"
      else
          \echo "Error: python version 3 is unavailable"
          return 64    
      fi     
    else
      \echo "Error: \$EMI_PYTHONBIN is not found in PATH"
      return 64
    fi
fi

emi_tmpVal=\`command -v \$EMI_PYTHONBIN\`
if [[ \$? -ne 0 ]] || [[ ! -e "\$emi_tmpVal" ]]; then
    \echo "Warning: \$EMI_PYTHONBIN does not seem to exist as a file'; unable to use as interpretor"
fi

emi_pythonVersion=\`\$EMI_PYTHONBIN -V 2>&1 | \awk '{print \$2}'\`
emi_pyVerMajor=\`\echo \$emi_pythonVersion | \cut -d "." -f 1\`
emi_pyVerMinor=\`\echo \$emi_pythonVersion | \cut -d "." -f 2\`
for emi_libDir in lib lib64; do
    emi_pyLibPath=\$EMI_TARBALL_BASE/usr/\$emi_libDir/python\$emi_pyVerMajor.\$emi_pyVerMinor/site-packages
    if [ -e \$emi_pyLibPath ]; then
	if [ -z \$PYTHONPATH ]; then
            export PYTHONPATH=\$emi_pyLibPath
	else
            export PYTHONPATH=\$emi_pyLibPath:\$PYTHONPATH   
	fi
    else
	\echo "Warning: python \$emi_pyVerMajor.\$emi_pyVerMinor bindings not found in \\\$EMI_TARBALL_BASE/usr/\$emi_libDir/"
    fi
done

if [ -z \$PERL5LIB ]; then
  export PERL5LIB=\$EMI_TARBALL_BASE/usr/lib64/perl5/vendor_perl
else
  export PERL5LIB=\$PERL5LIB:\$EMI_TARBALL_BASE/usr/lib64/perl5/vendor_perl
fi
export PERL5LIB="\$PERL5LIB:\$EMI_TARBALL_BASE/usr/lib/perl5/vendor_perl:\$EMI_TARBALL_BASE/usr/share/perl5:\$EMI_TARBALL_BASE/usr/share/perl5/vendor_perl"

export MANPATH=\$EMI_TARBALL_BASE/usr/share/man:\$MANPATH

export GFAL_PLUGIN_DIR=\$EMI_TARBALL_BASE/usr/lib64/gfal2-plugins/
export GFAL_CONFIG_DIR=\$EMI_TARBALL_BASE/etc/gfal2.d/

if [ "\$emi_pyVerMajor" = "3" ]; then
  if [ ! -e "/lib64/libpython3.6m.so.1.0" ]; then
    export LD_LIBRARY_PATH="\$LD_LIBRARY_PATH:\$EMI_TARBALL_BASE/usr/local/lib64"
  fi 
fi

if [ -z "\$X509_USER_PROXY" ]; then
  export X509_USER_PROXY=/tmp/x509up_u\`id -u\`
fi

export EMI_TARBALL_JAVACLASSPATH="$EMI_TARBALL_JAVACLASSPATH"

unset emi_pythonVersion emi_pyVerMajor emi_pyVerMinor emi_libVerMinor emi_pyLibPath emi_libDir emi_tmpVal

EOF

    return 0
}


alrb_fn_fixSymLinks()
{
    local alrb_file="$1"
    local alrb_filename=`basename $alrb_file`
    local alrb_fileDir=`\dirname $alrb_file`
    local alrb_newSymLink=".`readlink -m $alrb_file | \sed -e 's|'$PWD'||g'`"
    if [ -e "$alrb_newSymLink" ]; then
	local alrb_newSymLinkDir=`\dirname $alrb_newSymLink`
	local alrb_newSymlinkDirRel=`realpath --relative-to=$alrb_fileDir $alrb_newSymLinkDir`
	local alrb_newSymLinkFile=`basename $alrb_newSymLink`
	(
	    cd $alrb_fileDir
	    \rm $alrb_filename
	    ln -s $alrb_newSymlinkDirRel/$alrb_newSymLinkFile $alrb_filename
	)
    else
	\echo "Error: unable to determine correct fix for $alrb_file"
	return 64
    fi

    return 0
}


alrb_fn_findBrokenSymLinks()
{
    
    local alrb_file
    local alrb_realfile
    local alrb_tmpAr=( `\find . -type l  -print` )
    local alrb_tmpStr="\sed -e 's|`pwd`||g' | \sed -e 's|^/.*||g'"
    for alrb_file in ${alrb_tmpAr[@]}; do
	alrb_realfile=`readlink -e $alrb_file`
	if [ $? -ne 0 ]; then
	    # broken sym links
	    alrb_fn_fixSymLinks $alrb_file
	    continue
	fi
	local alrb_relativeFile=`\echo $alrb_file | eval $alrb_tmpStr`
	if [ "$alrb_relativeFile" = "" ]; then
	    # sym links that look at a tree different from pwd
	    alrb_fn_fixSymLinks $alrb_file
	    continue
	fi
    done

    return 0
}


alrb_fn_defineATLASSpecifics()
{

    mkdir -p $alrb_srcHome/etc/vomses
#    \echo '"atlas" "lcg-voms2.cern.ch" "15001" "/DC=ch/DC=cern/OU=computers/CN=lcg-voms2.cern.ch" "atlas" "24"' > $alrb_srcHome/etc/vomses/atlas-lcg-voms2.cern.ch
#    \echo '"atlas" "voms2.cern.ch" "15001" "/DC=ch/DC=cern/OU=computers/CN=voms2.cern.ch" "atlas" "24"' > $alrb_srcHome/etc/vomses/atlas-voms2.cern.ch
#    \echo '"atlas" "voms-atlas-auth.app.cern.ch" "443" "/DC=ch/DC=cern/OU=computers/CN=atlas-auth.web.cern.ch" "atlas" "24"' > $alrb_srcHome/etc/vomses/atlas-voms-atlas-auth.app.cern.ch
    \echo '"atlas" "voms-atlas-auth.cern.ch" "443" "/DC=ch/DC=cern/OU=computers/CN=atlas-auth.cern.ch" "atlas" "24"' > $alrb_srcHome/etc/vomses/atlas-voms-atlas-auth.cern.ch

    
    mkdir -p $alrb_srcHome/etc/grid-security/vomsdir/atlas
    \echo '/DC=ch/DC=cern/OU=computers/CN=lcg-voms2.cern.ch
/DC=ch/DC=cern/CN=CERN Grid Certification Authority' > $alrb_srcHome/etc/grid-security/vomsdir/atlas/lcg-voms2.cern.ch.lsc
    \echo '/DC=ch/DC=cern/OU=computers/CN=voms2.cern.ch
/DC=ch/DC=cern/CN=CERN Grid Certification Authority' > $alrb_srcHome/etc/grid-security/vomsdir/atlas/voms2.cern.ch.lsc
    \echo '/DC=ch/DC=cern/OU=computers/CN=atlas-auth.web.cern.ch
/DC=ch/DC=cern/CN=CERN Grid Certification Authority' > $alrb_srcHome/etc/grid-security/vomsdir/atlas/voms-atlas-auth.app.cern.ch.lsc
    \echo '/DC=ch/DC=cern/OU=computers/CN=atlas-auth.cern.ch
/DC=ch/DC=cern/CN=CERN Grid Certification Authority' > $alrb_srcHome/etc/grid-security/vomsdir/atlas/voms-atlas-auth.cern.ch.lsc
    
    return 0

}


alrb_fn_fixJavaVomsClients()
{

    local alrb_tmpAr=( `\find usr/bin -name voms-* -type f` )
    local alrb_file
    for alrb_file in ${alrb_tmpAr[@]}; do
	\sed -e 's|VOMSCLIENTS_LIBS=/|VOMSCLIENTS_LIBS=\$EMI_TARBALL_BASE/|' -e 's|-cp \$(build-classpath [[:alnum:]].*) |-cp \$EMI_TARBALL_JAVACLASSPATH |g' -e '2 i source \$EMI_TARBALL_BASE/localJavaSetup.sh' $alrb_file > $alrb_file.new
	\mv $alrb_file.new $alrb_file 
	chmod +x $alrb_file
    done

    cd usr/bin
    if [ ! -e voms-proxy-init ]; then
	ln -s ./voms-proxy-init3 voms-proxy-init
    fi
    if [ ! -e voms-proxy-info ]; then
	ln -s ./voms-proxy-info3 voms-proxy-info
    fi
    if [ ! -e voms-proxy-destroy ]; then
	ln -s ./voms-proxy-destroy3 voms-proxy-destroy
    fi

    alrb_file=`readlink -f voms-proxy-init`
    \sed -e '2 i \\\echo $@ | \\\grep -e "-vomses" > /dev/null 2>&1\nif [ $? -ne 0 ]; then\n  set -- "$@" "--vomses" "$X509_VOMSES"\nfi' $alrb_file > $alrb_file.new
    \mv $alrb_file.new $alrb_file
    chmod +x $alrb_file    
    
    return 0
}


#! * * * main * * * 

alrb_version="`date +%y.%m.%d`-snapshot"

if [ ! -z $BASH_SOURCE ]; then
    alrb_packageToolHome=`\dirname ${BASH_SOURCE[0]}`
else
    alrb_packageToolHome=`\dirname $0`
fi
if [ "$alrb_packageToolHome" = "." ]; then
    alrb_packageToolHome=`pwd`
fi

source $alrb_packageToolHome/setupEnvironment.sh $alrb_toolName $alrb_version
if [ $? -ne 0 ]; then
    exit 64
fi

cd $alrb_workdir

\mkdir -p $alrb_workdir/rpms
cd $alrb_workdir/rpms
alrb_rpms="gfal2-all xrootd-client voms-clients-java voms-api-java python3-gfal2-util fetch-crl globus-gsi-cert-utils-progs globus-proxy-utils myproxy"
alrb_devRepo=""
if [ "$alrb_osver" = "9" ]; then
# gfal2 needed here because container has gfal2-all (dev tools)
    alrb_rpms="$alrb_rpms gsoap nordugrid-arc-client gfal2"
    if [ "$alrb_unameM" = "aarch64" ]; then
	alrb_rpms="$alrb_rpms openldap-compat"
    fi
    alrb_rpms="$alrb_rpms nordugrid-arc-plugins-arcrest nordugrid-arc-plugins-gridftpjob"
    alrb_devRepo=" $alrb_devRepo --enablerepo epel-testing"
elif [ "$alrb_osver" = "8" ]; then
# gfal2 needed here because container has gfal2-all (dev tools)
    alrb_rpms="$alrb_rpms nordugrid-arc-client gfal2"
    alrb_rpms="$alrb_rpms nordugrid-arc-plugins-arcrest nordugrid-arc-plugins-gridftpjob"
elif [ "$alrb_osver" = "7" ]; then
    alrb_rpms="$alrb_rpms python2-gfal2 python2-gfal2-util nordugrid-arc6-client"
    alrb_rpms="$alrb_rpms nordugrid-arc6-plugins-arcrest nordugrid-arc6-plugins-gridftpjob"
    alrb_devRepo=" $alrb_devRepo --enablerepo epel-testing"
fi

which yumdownloader > /dev/null 2>&1
if [ $? -eq 0 ]; then
    yumdownloader --resolve $alrb_devRepo $alrb_rpms
else
    dnf download --resolve $alrb_devRepo $alrb_rpms
fi

# temporary fix for bad voms-clients-java
cd $alrb_workdir/rpms
\ls $alrb_workdir/rpms/voms-clients-java-* | grep -e 3.3.2 -e 3.3.3 > /dev/null 2>&1
if [ $? -eq 0 ]; then
# no arch so pick x86_64 since aaarch64 is not there ...
    alrb_urlVomsClient="https://linuxsoft.cern.ch/wlcg/$alrb_osFlavor$alrb_osver/x86_64/voms-clients-java-3.3.4-1.el$alrb_osver.noarch.rpm"
    \echo "Fixing voms-clients-java with $alrb_urlVomsClient ..."
    \rm -f $alrb_workdir/rpms/voms-clients-java-3.3.3* $alrb_workdir/rpms/voms-clients-java-3.3.2*
    $ATLAS_LOCAL_ROOT_BASE/utilities/wgetFile.sh $alrb_urlVomsClient
fi
\ls $alrb_workdir/rpms/voms-api-java-3.3.2* > /dev/null 2>&1
if [ $? -eq 0 ]; then
# no arch so pick x86_64 since aaarch64 is not there ...
    alrb_urlVomsClient="https://linuxsoft.cern.ch/wlcg/$alrb_osFlavor$alrb_osver/x86_64/voms-api-java-3.3.3-1.el7.noarch.rpm"
    \echo "Fixing voms-clients-java with $alrb_urlVomsClient ..."
    \rm -f $alrb_workdir/rpms/voms-api-java-3.3.2*
    $ATLAS_LOCAL_ROOT_BASE/utilities/wgetFile.sh $alrb_urlVomsClient
fi

alrb_srcHome=$alrb_workdir/build
\mkdir -p $alrb_srcHome
cd $alrb_srcHome

ls -1 $alrb_workdir/rpms > ./os-rpms.txt
\cat /etc/os-release > ./buildmachineinfo.txt
\echo "uname=`uname -a`" >> ./buildmachineinfo.txt
\echo "HEP_OSlibs=`rpm -qa | \grep -e HEP_OSlibs`" >> ./buildmachineinfo.txt

for alrb_item in `ls -1 $alrb_workdir/rpms/*.rpm`; do
    rpm2cpio $alrb_item | cpio -dim;
done

alrb_result=`\find usr/lib/jvm -name java -type f | \tail -n 1`
if [[ "$alrb_result" != "" ]] && [[ -e $alrb_result ]]; then
    alrb_result=`\echo $alrb_result | \sed -e 's|/bin/java||g' `
    \cat <<EOF > ./localJavaPath.sh 
export EMI_OVERRIDE_JAVA_HOME="\$EMI_TARBALL_BASE/$alrb_result"
EOF
    \cat <<EOF > ./localJavaSetup.sh
if [ ! -z "\$EMI_OVERRIDE_JAVA_HOME" ]; then 
  export JAVA_HOME="\$EMI_OVERRIDE_JAVA_HOME"
  export JAVACONFDIRS="\$EMI_TARBALL_BASE/etc/java"
  export PATH="\$JAVA_HOME/bin:$PATH"
fi    
EOF
fi
EMI_TARBALL_JAVACLASSPATH="`\find usr/share -name *.jar | \awk '{print "$EMI_TARBALL_BASE/"$1""}' | \tr '\n' ':'`"
\sed  's|/usr|\$EMI_TARBALL_BASE/usr|g' etc/java/java.conf > etc/java/java.conf.new
\mv etc/java/java.conf.new etc/java/java.conf

cd $alrb_srcHome
alrb_fn_createSetupScript

cd $alrb_srcHome
if [ "$alrb_osver" = "7" ]; then
    if [[ -e /lib64/libpython3.6m.so.1.0 ]] && [[ ! -e $alrb_srcHome/usr/local/lib64/libpython3.6m.so.1.0 ]]; then
	\mkdir -p $alrb_srcHome/usr/local/lib64
	\cp /lib64/libpython3.6m.so.1.0 $alrb_srcHome/usr/local/lib64/
    fi				    
fi

# we need python 3.10 compatibility link for big panda and el9 ...
if [ "$alrb_osver" = "9" ]; then
    alrb_pythonArSL=( ${alrb_pythonAr[@]}
	"3.10"
    )
else
    alrb_pythonArSL=( ${alrb_pythonAr[@]} )
fi
for alrb_libDir in lib64 lib; do
    cd $alrb_srcHome/usr/$alrb_libDir
    for alrb_item in ${alrb_pythonArSL[@]}; do		
	if [ -e python$alrb_item ]; then
	    continue
	fi
	let alrb_tmpMinorVer=`\echo $alrb_item | \cut -f 2 -d '.'`
	while [[ ! -d ./python3.$alrb_tmpMinorVer ]] && [[ $alrb_tmpMinorVer -gt 6 ]] ; do
	    let alrb_tmpMinorVer=$alrb_tmpMinorVer-1
	done
	if [ -d python3.$alrb_tmpMinorVer ]; then
	    ln -s python3.$alrb_tmpMinorVer python$alrb_item
	fi
    done
done

cd $alrb_srcHome
alrb_fn_findBrokenSymLinks

cd $alrb_srcHome
alrb_fn_defineATLASSpecifics

cd $alrb_srcHome
alrb_fn_fixJavaVomsClients

cd $alrb_srcHome
\cat <<EOF > ./_README_FIRST.txt 
This package has only ATLAS defined voms
To use, define EMI_TARBALL_BASE to point to this top level dir.
EOF

cd $alrb_srcHome
\echo "Fixing as per Petr's instructions:
It is necessary to change default gfal2 configuration option RETRIEVE_BEARER_TOKEN=false in the http_plugin.conf, because otherwise 'rucio upload' would fail with DPM storages (we have to wait for dmlite 1.15.2 deployment)."
if [ -e etc/gfal2.d/http_plugin.conf ]; then
    \sed -e 's|RETRIEVE_BEARER_TOKEN=true|RETRIEVE_BEARER_TOKEN=false|g' etc/gfal2.d/http_plugin.conf > etc/gfal2.d/http_plugin.conf.tmp
    \mv etc/gfal2.d/http_plugin.conf.tmp etc/gfal2.d/http_plugin.conf
fi

cd $alrb_srcHome
alrb_fn_createPriorityBinDir

cd $alrb_srcHome
alrb_oldHome=$HOME
alrb_gfalVer="`\grep -i -e '^python3-gfal2-[[:digit:]\.]*-' $alrb_srcHome/os-rpms.txt | \sed -e 's|^python3-gfal2-||g' | \sed -e 's|-.*||g'`"
let alrb_gfalVerN=`$ATLAS_LOCAL_ROOT_BASE/utilities/convertToDecimal.sh $alrb_gfalVer  3`
if [ $alrb_gfalVerN -ge 11300 ]; then
    alrb_fn_buildPythonBindings
else
    alrb_fn_buildPythonBindingsPre113
fi
if [ $? -ne 0 ]; then
    exit 64
fi
export HOME=$alrb_oldHome

cd $alrb_srcHome
\find $alrb_srcHome  -type d -exec chmod +w  {} \;
gtar zcf $alrb_workdir/$alrb_tag.tgz *
\echo "Ready: $alrb_workdir/$alrb_tag.tgz"

exit 0
