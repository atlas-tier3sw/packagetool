#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! make-gitlab.sh
#!
#! create a tarball for gitlab
#!
#! Usage:
#!     make-gitlab.sh <version>
#!  should be run inside a container for the os/arch that you need
#!  releases are from
#!       https://python-gitlab.readthedocs.io/en/stable/index.html
#!       https://github.com/python-gitlab/python-gitlab
#!
#! History:
#!    15Aug22: A. De Silva
#!
#!----------------------------------------------------------------------------

alrb_toolName="gitlab"

if [ ! -z $BASH_SOURCE ]; then
    alrb_packageToolHome=`\dirname ${BASH_SOURCE[0]}`
else
    alrb_packageToolHome=`\dirname $0`
fi
if [ "$alrb_packageToolHome" = "." ]; then
    alrb_packageToolHome=`pwd`
fi

if [ "$#" -ne 1 ]; then 
    \echo "Usage: make-$alrb_toolName.sh <version>"
    \echo "       where version is one from https://github.com/python-gitlab/python-gitlab"
    exit 64
fi
alrb_version="$1"

source $alrb_packageToolHome/setupEnvironment.sh $alrb_toolName $alrb_version
if [ $? -ne 0 ]; then
    exit 64
fi

cd $alrb_workdir

\echo "python-gitlab==$alrb_version" > $alrb_workdir/requirements.txt

for alrb_pyVersionVal in ${alrb_pythonAr[@]}; do
    (

	if [ -e $alrb_workdir/logs/minPython3.sh ]; then
	    source $alrb_workdir/logs/minPython3.sh
	else
	    let python_gitlab_builtWithPyN0=9999999
	fi

	alrb_pyALRBVersion="$alrb_osFlavor$alrb_osver-$alrb_pyVersionVal"
	\echo "
	      
 * * * Installing $alrb_tag, python $alrb_pyVersionVal ***
"		
	alrb_logdir=$alrb_workdir/logs/$alrb_pyVersionVal
	\mkdir -p $alrb_logdir
	if [ $? -ne 0 ]; then
	    exit 64
	fi
	
	type -a lsetup > /dev/null 2>&1 
	if [ $? -ne 0 ]; then
	    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
	    if [ $? -ne 0 ]; then
		exit 64
	    fi
	fi
	cd $alrb_workdir
	$ATLAS_LOCAL_ROOT_BASE/utilities/make-genericPip.sh $alrb_workdir/requirements.txt "$alrb_pyVersionVal"
	if [ $? -ne 0 ]; then
	    \echo "FAILED: OS: $alrb_osver, ARCH: $alrb_unameM, PYTHON: $alrb_pyVersionVal" | tee -a $alrb_workdir/status
	    \echo "        Will continue with next ..."
	    continue
	else
	    \echo "OK: OS: $alrb_osver, ARCH: $alrb_unameM, PYTHON: $alrb_pyVersionVal" |  tee -a $alrb_workdir/status
	    \echo "$alrb_pyVersionVal|$alrb_pyALRBVersion|" >> $alrb_workdir/pythonBuild.txt
	fi	    

	lsetup -f "python $alrb_pyALRBVersion"	
	alrb_tmpVal=`python3 -c "import platform; print(platform.python_version())"`
	let alrb_tmpValN=`$ATLAS_LOCAL_ROOT_BASE/utilities/convertToDecimal.sh $alrb_tmpVal 3`
	if [ $alrb_tmpValN -lt $python_gitlab_builtWithPyN0 ]; then
	    \rm -f $alrb_workdir/logs/minPython3.sh
	    \echo "
python_gitlab_builtWithPyN0=$alrb_tmpValN
python_gitlab_builtWithPy=$alrb_tmpVal 
python_gitlab_builtWithPyN=$alrb_tmpValN     
"  > $alrb_workdir/logs/minPython3.sh
	    let python_gitlab_builtWithPyN0=$alrb_tmpValN 
	fi
	
    )
    
done

cd $alrb_workdir
ln -s logs logDir
gtar zcf $alrb_workdir/../$alrb_tag.tar.gz *
\echo "Ready: $alrb_workdir/../$alrb_tag.tar.gz"

exit 0
