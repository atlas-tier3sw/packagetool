#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! make-logstash.sh
#!
#! create a tarball for logstash
#!
#! Usage:
#!     make-logstash.sh <version> 
#!  should be run inside a container for the os/arch that you need.
#!
#! History:
#!    07Mar22: A. De Silva
#!
#!----------------------------------------------------------------------------

alrb_toolName="logstash"

if [ ! -z $BASH_SOURCE ]; then
    alrb_packageToolHome=`\dirname ${BASH_SOURCE[0]}`
else
    alrb_packageToolHome=`\dirname $0`
fi
if [ "$alrb_packageToolHome" = "." ]; then
    alrb_packageToolHome=`pwd`
fi

if [ "$#" -ne 1 ]; then 
    \echo " 
Usage: make-$alrb_toolName.sh <version>
       where <version> is one from
         https://github.com/eht16/python-logstash-async
"
    exit 64
fi
alrb_version="$1"

source $alrb_packageToolHome/setupEnvironment.sh $alrb_toolName $alrb_version
if [ $? -ne 0 ]; then
    exit 64
fi

cd $alrb_workdir

\echo "wrapt" > $alrb_workdir/requirements.txt
\echo "python-logstash-async==$alrb_version" >> $alrb_workdir/requirements.txt

for alrb_pyVersionVal in ${alrb_pythonAr[@]}; do
    (
	alrb_pyALRBVersion="$alrb_osFlavor$alrb_osver-$alrb_pyVersionVal"
	\echo "
	
 * * * Installing $alrb_tag, python $alrb_pyVersionVal ***
"		
	alrb_logdir=$alrb_workdir/logs/$alrb_pyVersionVal
	\mkdir -p $alrb_logdir
	if [ $? -ne 0 ]; then
	    exit 64
	fi
	
	type -a lsetup > /dev/null 2>&1 
	if [ $? -ne 0 ]; then
	    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
	    if [ $? -ne 0 ]; then
		exit 64
	    fi
	fi
	cd $alrb_workdir
	$ATLAS_LOCAL_ROOT_BASE/utilities/make-genericPip.sh $alrb_workdir/requirements.txt "$alrb_pyVersionVal"
	if [ $? -ne 0 ]; then
	    \echo "FAILED: OS: $alrb_osver, ARCH: $alrb_unameM, PYTHON: $alrb_pyVersionVal" | tee -a $alrb_workdir/status
	    \echo "        Will continue with next ..."
	    continue
	else
	    \echo "OK: OS: $alrb_osver, ARCH: $alrb_unameM, PYTHON: $alrb_pyVersionVal" |  tee -a $alrb_workdir/status
	    \echo "$alrb_pyVersionVal|$alrb_pyALRBVersion|" >> $alrb_workdir/pythonBuild.txt
	fi	    
    )
    
done

cd $alrb_workdir
gtar zcf $alrb_workdir/../$alrb_tag.tar.gz *
\echo "Ready: $alrb_workdir/../$alrb_tag.tar.gz"

exit 0
B
