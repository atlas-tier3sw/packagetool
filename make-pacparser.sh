#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! make-pacparser.sh
#!
#! create a tarball for davix
#!
#! Usage:
#!     make-pacparser.sh <version>
#!  should be run inside a container for the os/arch that you need
#!  The version is a tag from https://github.com/manugarg/pacparser
#!
#! History:
#!    28Feb23: A. De Silva
#!
#!----------------------------------------------------------------------------

alrb_toolName="pacparser"

alrb_fn_createSetupScript()
{
    \cat <<EOF > $alrb_buildDir/setup.sh

export PACPARSER_INSTALLEDDIR="\`dirname \${BASH_SOURCE:-\$0}\`"

export PATH="\$PATH:\$PACPARSER_INSTALLEDDIR/bin"

if [ -z \$LD_LIBRARY_PATH ]; then
  export LD_LIBRARY_PATH="\$PACPARSER_INSTALLEDDIR/lib"
else
  export LD_LIBRARY_PATH="\$LD_LIBRARY_PATH:\$PACPARSER_INSTALLEDDIR/lib"
fi

EOF
    return 0
}

if [ ! -z $BASH_SOURCE ]; then
    alrb_packageToolHome=`\dirname ${BASH_SOURCE[0]}`
else
    alrb_packageToolHome=`\dirname $0`
fi
if [ "$alrb_packageToolHome" = "." ]; then
    alrb_packageToolHome=`pwd`
fi

if [ "$#" -ne 1 ]; then 
    \echo "Usage: make-$alrb_toolName.sh <version>"
    \echo "       where version is a tag from https://github.com/manugarg/pacparser"
    exit 64
fi
alrb_version="$1"

source $alrb_packageToolHome/setupEnvironment.sh $alrb_toolName $alrb_version
if [ $? -ne 0 ]; then
    exit 64
fi

cd $alrb_workdir

git clone https://github.com/manugarg/pacparser.git pacparser
if [ $? -ne 0 ]; then
    exit 64
fi
cd pacparser
git checkout tags/$alrb_version
if [ $? -ne 0 ]; then
    exit 64
fi

alrb_buildDir="$alrb_workdir/build"
mkdir -p $alrb_buildDir
export PREFIX=$alrb_buildDir

alrb_logDir="$alrb_buildDir/logDir"
mkdir -p $alrb_logDir

make -C src 2>&1 | tee $alrb_logDir/make.log
if [ $? -ne 0 ]; then
    exit 64
fi

make -C src install 2>&1 | tee $alrb_logDir/make-install.log
if [ $? -ne 0 ]; then
    exit 64
fi

cd $alrb_buildDir
alrb_fn_createSetupScript

gtar zcf $alrb_workdir/$alrb_tag.tar.gz *
\echo "Ready: $alrb_workdir/$alrb_tag.tar.gz"

exit 0
