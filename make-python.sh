#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! make-python.sh
#!
#! create a tarball for python
#!
#! Usage:
#!     make-python.sh <version>
#!  should be run inside a container for the os/arch that you need.
#!
#! History:
#!    07Mar22: A. De Silva
#!
#!----------------------------------------------------------------------------

alrb_toolName="python"

if [ ! -z $BASH_SOURCE ]; then
    alrb_packageToolHome=`\dirname ${BASH_SOURCE[0]}`
else
    alrb_packageToolHome=`\dirname $0`
fi
if [ "$alrb_packageToolHome" = "." ]; then
    alrb_packageToolHome=`pwd`
fi

if [ "$#" -lt 1 ]; then 
    \echo "Usage: make-$alrb_toolName.sh <version> [add_requests=<version>]"
    exit 64
fi
alrb_version="$1"

source $alrb_packageToolHome/setupEnvironment.sh $alrb_toolName $alrb_version
if [ $? -ne 0 ]; then
    exit 64
fi

cd $alrb_workdir

alrb_majorVersion=`\echo $alrb_version | \cut -f 1 -d "."`

alrb_durl="https://www.python.org/ftp/python/$alrb_version/Python-$alrb_version.tgz"

mkdir -p $alrb_workdir/build
mkdir -p $alrb_workdir/source
mkdir -p $alrb_workdir/build/buildLogs

alrb_opensslBuildOpts=""
# python 3.10 and newer on centos7 needs openssl 1.1.1 or newer
# https://docs.python.org/3.10/using/unix.html#custom-openssl
if [ "$alrb_osver" = "7" ]; then
    let alrb_tmpValN=`\echo $alrb_version | \cut -f 2 -d "."`
    if [ $alrb_tmpValN -ge 10 ]; then
	alrb_opsnsslVer="1.1.1"
	alrb_opensslDir="$alrb_workdir/openssl"
	alrb_opensslBuild="$alrb_workdir/openssl/build"
	alrb_opensslBuildOpts="-with-openssl=$alrb_opensslBuild --with-openssl-rpath=auto"
	\mkdir -p $alrb_opensslBuild
	\echo "
Building local openssl $alrb_opsnsslVer ..."
	(
	    cd $alrb_opensslDir
	    curl -O https://www.openssl.org/source/openssl-$alrb_opsnsslVer.tar.gz 2>&1 | tee $alrb_opensslDir/openssl.log
	    tar xzf openssl-$alrb_opsnsslVer.tar.gz
	    cd openssl-$alrb_opsnsslVer
	    ./config \
		--prefix=$alrb_opensslBuild \
		--libdir=lib \
		--openssldir=`find /etc/ -name openssl.cnf -printf "%h\n" 2>/dev/null` 2>&1 | tee $alrb_opensslDir/openssl.log
	    make -j1 depend 2>&1 | tee $alrb_opensslDir/openssl.log
	    make -j8 2>&1 | tee $alrb_opensslDir/openssl.log
	    make install_sw 2>&1 | tee $alrb_opensslDir/openssl.log
	)
    fi
fi

(
    cd $alrb_workdir
    curl -O $alrb_durl
    if [ $? -ne 0 ]; then
	exit 64
    fi

    alrb_tmpVal=`\find . -maxdepth 1 -name *.tgz`
    cd $alrb_workdir/source
    gtar zxf ../$alrb_tmpVal
    cd $alrb_workdir/source/Python-$alrb_version
    
    ./configure $alrb_opensslBuildOpts --prefix $alrb_workdir/build 2>&1 | tee $alrb_workdir/build/buildLogs/config.log 
    if [ $? -ne 0 ]; then
	exit 64
    fi
    make 2>&1 | tee $alrb_workdir/build/buildLogs/make.log
    if [ $? -ne 0 ]; then
	exit 64
    fi
    make test 2>&1 | tee $alrb_workdir/build/buildLogs/make-test.log
    if [ $? -ne 0 ]; then
	\echo "Tests failed.  Check before deployment."
	#    exit 64
    fi
    make install 2>&1 | tee $alrb_workdir/build/buildLogs/make-install.log
    if [ $? -ne 0 ]; then
	exit 64
    fi
)


# oher packages needed
(
    cd $alrb_workdir
    export PATH="$alrb_workdir/build/bin:$PATH"
    export LD_LIBRARY_PATH="$alrb_workdir/build/lib"
    export LIBRARY_PATH="$alrb_workdir/build/lib"
    export SSL_CERT_DIR="$ATLAS_LOCAL_ROOT_BASE/etc/grid-security-emi/certificates"

    if [ "$alrb_majorVersion" = "2" ]; then
	python$alrb_majorVersion  -m ensurepip --upgrade
    fi
    
    # python requests
    python$alrb_majorVersion -m pip install requests | tee $alrb_workdir/build/buildLogs/requests.log
    if [ $? -ne 0 ]; then
	exit 64
    fi
)

# fix paths
(
    cd $alrb_workdir/build
    alrb_postFixFileAr=( `\grep -I -r -e "$alrb_workdir/build" * | \egrep -v -e "^buildLogs" | \cut -f 1 -d ":" | sort -u` )

    \cat <<EOF > postInstallFix.sh
let alrb_tmpRC=0
alrb_postFixFileAr=( ${alrb_postFixFileAr[@]} )
for alrb_item in \${alrb_postFixFileAr[@]}; do
    \sed -e 's|$alrb_workdir/build|'\$alrb_InstallDir'|g' \$alrb_item > \$alrb_item.new
    if [ \$? -eq 0 ]; then
      \echo "  patching \$alrb_item ..."
      chmod --reference \$alrb_item \$alrb_item.new
      \mv \$alrb_item.new \$alrb_item
    else  
      \echo "  Error: Patching failed for \$alrb_item"
      let alrb_tmpRC=64 
    fi  
done

return \$alrb_tmpRC    	  	     

EOF
    
)


cd $alrb_workdir/build
gtar zcf ../$alrb_tag.tgz *
\echo "Ready: $alrb_workdir/$alrb_tag.tgz"

exit 0
