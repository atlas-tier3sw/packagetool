#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! make-scikit-hep.sh
#!
#! create a tarball for scikit-hep
#!
#! Usage:
#!     make-scikit-hep.sh <version> 
#!  where <versions> are tags from the scikit-hep github ...
#!  This should be run inside a container for the os/arch that you need
#!  releases are from
#!       https://github.com/scikit-hep/scikit-hep
#!
#! History:
#!    21Mar23: A. De Silva
#!
#!----------------------------------------------------------------------------

alrb_toolName="scikit-hep"

alrb_fn_createSetupScript()
{

    \cat <<EOF >> $alrb_workdir/setup.sh
if [ ! -z \$BASH_SOURCE ]; then
    alrb_scriptHome=\`\dirname \${BASH_SOURCE[0]}\`
else
    alrb_scriptHome=\`\dirname \$0\`
fi
if [ "\$alrb_scriptHome" = "." ]; then
    alrb_scriptHome=\`pwd\`
fi

EOF

    local alrb_dirList=(
	"PATH:/usr/bin"
	"PATH:/bin"
	"PATH:/usr/sbin"
	"PATH:/sbin"
	"LD_LIBRARY_PATH:/usr/lib"
	"LD_LIBRARY_PATH:/usr/lib64"
	"LD_LIBRARY_PATH:/lib"
	"LD_LIBRARY_PATH:/lib64"
    )

    local alrb_item alrb_item2
    local alrb_donePyPath=","
    for alrb_item in ${alrb_dirList[@]}; do
	local alrb_pathname=`\echo $alrb_item | \cut -f 1 -d ":"`
	local alrb_path=`\echo $alrb_item | \cut -f 2 -d ":"`
	if [ -d "$alrb_workdir$alrb_path" ]; then
	    \echo "appendPath $alrb_pathname \$alrb_scriptHome$alrb_path" >> $alrb_workdir/setup.sh.tmp

	    if [ "$alrb_pathname" = "LD_LIBRARY_PATH" ]; then
		local alrb_tmpAr=( `\find $alrb_workdir$alrb_path -name site-packages -type d` )
		for alrb_item2 in ${alrb_tmpAr[@]}; do
		    \echo "$alrb_donePyPath" | \grep -e "$alrb_path" > /dev/null 2>&1
		    if [ $? -eq 0 ]; then
			continue
		    fi
		    \echo "		    		    
if [ -d \$alrb_scriptHome$alrb_path/python\$alrb_scriptPython ]; then
    appendPath PYTHONPATH \$alrb_scriptHome$alrb_path/python\$alrb_scriptPython
fi
if [ -d \$alrb_scriptHome$alrb_path/python\$alrb_scriptPython/site-packages ]; then
    appendPath PYTHONPATH \$alrb_scriptHome$alrb_path/python\$alrb_scriptPython/site-packages
fi
" >> $alrb_workdir/setup.sh.tmp2	
		    alrb_donePyPath="$alrb_donePyPath,$alrb_path,"
		done
		
	    fi
	fi
    done    

    let alrb_nPythonVer=`\wc -l $alrb_workdir/pythonBuild.txt | \cut -f 1 -d ' '`

    if [ $alrb_nPythonVer -gt 1 ]; then
	\cat <<EOF >> $alrb_workdir/setup.sh
if [ "\$#" -ne 1 ]; then
  \echo "There is more than one python version so this is ambigious ..."
  \echo "Error: You need to specify an argument for python version, available:"
  \cat \$alrb_scriptHome/pythonBuild.txt | \cut -f 1 -d "|"  

  return 64
else
  alrb_scriptPython="\$1"
fi

EOF
    else
	\cat <<EOF >> $alrb_workdir/setup.sh
alrb_scriptPython="\`\tail -n 1 \$alrb_scriptHome/pythonBuild.txt | \cut -f 1 -d '|'\`"  			
EOF
    fi


    \cat <<EOF >> $alrb_workdir/setup.sh

alrb_tmpVer="\`\grep -e \"^\$alrb_scriptPython\" \$alrb_scriptHome/pythonBuild.txt | \cut -f 2 -d '|'\`"
if [ \$? -eq 0 ]; then

  alrb_thisPy3=""
  alrb_tmpVal="\`python3 -V 2>&1\`"
  if [ \$? -eq 0 ]; then
     alrb_thisPy3=\`\echo \$alrb_tmpVal | \cut -f 2 -d " " | \cut -f -2 -d "."\`
  fi
  if [ "\$alrb_thisPy3" != "\$alrb_scriptPython" ]; then
    lsetup -f "python \$alrb_tmpVer"
  fi
fi

EOF

    touch $alrb_workdir/setup.sh.tmp
    \cat $alrb_workdir/setup.sh.tmp >> $alrb_workdir/setup.sh  
    \rm -f $alrb_workdir/setup.sh.tmp
    touch $alrb_workdir/setup.sh.tmp2
    \cat $alrb_workdir/setup.sh.tmp2 >> $alrb_workdir/setup.sh  
    \rm -f $alrb_workdir/setup.sh.tmp2
    
    \cat <<EOF >> $alrb_workdir/setup.sh
unset alrb_scriptHome alrb_scriptPython alrb_tmpVer alrb_tmpVal alrb_thisPy3
EOF

    return 0
}

# main

if [ ! -z $BASH_SOURCE ]; then
    alrb_packageToolHome=`\dirname ${BASH_SOURCE[0]}`
else
    alrb_packageToolHome=`\dirname $0`
fi
if [ "$alrb_packageToolHome" = "." ]; then
    alrb_packageToolHome=`pwd`
fi

if [ "$#" -ne 1 ]; then 
    \echo "
Usage: make-$alrb_toolName.sh
       where <version> is one from
       	 https://github.com/scikit-hep/scikit-hep
"
    exit 64
fi
alrb_version="$1"

source $alrb_packageToolHome/setupEnvironment.sh $alrb_toolName $alrb_version
if [ $? -ne 0 ]; then
    exit 64
fi

# override - only support python3.9 for now (3.11 is too new)
\echo " *  *  * 
Warning: Only python 3.9 will be built.
                   This is overriden in make-scikit-hep.sh "
alrb_pythonAr=( "3.9" )
\echo " *  *  * "

touch $alrb_workdir/requirements.txt 
#\echo "wheel" >> $alrb_workdir/requirements.txt
\echo "scikit-hep==$alrb_version" >> $alrb_workdir/requirements.txt

for alrb_pyVersionVal in ${alrb_pythonAr[@]}; do
    (
	\echo "
	
 * * * Installing $alrb_tag, python $alrb_pyVersionVal ***
"	
	alrb_logdir=$alrb_workdir/logs/$alrb_pyVersion
	\mkdir -p $alrb_logdir
	if [ $? -ne 0 ]; then
	    exit 64
	fi

	type -a lsetup > /dev/null 2>&1 
	if [ $? -ne 0 ]; then
	    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
	    if [ $? -ne 0 ]; then
		exit 64
	    fi
	fi
	cd $alrb_workdir
	$ATLAS_LOCAL_ROOT_BASE/utilities/make-genericPip.sh $alrb_workdir/requirements.txt "$alrb_pyVersionVal"
	if [ $? -ne 0 ]; then
	    \echo "FAILED: OS: $alrb_osver, ARCH: $alrb_unameM, PYTHON: $alrb_pyVersion" | tee -a $alrb_workdir/status
	    \echo "        Will continue with next ..."
	    continue
	else
	    \echo "OK: OS: $alrb_osver, ARCH: $alrb_unameM, PYTHON: $alrb_pyVersionVal" |  tee -a $alrb_workdir/status
	    \echo "$alrb_pyVersion|$alrb_pyALRBVersion|" >> $alrb_workdir/pythonBuild.txt
	fi	    
    )

done

cd $alrb_workdir
alrb_fn_createSetupScript

cd $alrb_workdir
gtar zcf $alrb_workdir/../$alrb_tag.tar.gz *
\echo "Ready: $alrb_workdir/../$alrb_tag.tar.gz"

exit 0
