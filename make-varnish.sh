#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! make-varnish.sh
#!
#! Installs a version of varnish from
#!  https://varnish-cache.org/releases/index.html
#!
#! Usage:
#!     make-varnish.sh <version>
#!
#! History:
#!    16Aug24: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_toolName="varnish"

alrb_version="$1"

if [ ! -z $BASH_SOURCE ]; then
    alrb_packageToolHome=`\dirname ${BASH_SOURCE[0]}`
else
    alrb_packageToolHome=`\dirname $0`
fi
if [ "$alrb_packageToolHome" = "." ]; then
    alrb_packageToolHome=`pwd`
fi

source $alrb_packageToolHome/setupEnvironment.sh $alrb_toolName $alrb_version
if [ $? -ne 0 ]; then
    exit 64
fi

cd $alrb_workdir

alrb_repoVerNum=`\echo $alrb_version | \cut -f -2 -d "." | \sed -e 's|\.||g'`
alrb_repoName="varnishcache_varnish$alrb_repoVerNum"
\rm -f $alrb_workdir/$alrb_repoName.repo
# get the repo ...
curl -o $alrb_workdir/$alrb_repoName.repo "https://packagecloud.io/install/repositories/varnishcache/varnish${alrb_repoVerNum}/config_file.repo?os=el&dist=${alrb_osver}&source=script"

alrb_rpms="varnish-${alrb_version}*${alrb_unameM}"
alrb_devRepo="--enablerepo=varnishcache_varnish$alrb_repoVerNum"

alrb_srcHome=$alrb_workdir/build
\mkdir -p $alrb_srcHome

\cat <<EOF >> $alrb_workdir/payload.sh

\mkdir -p $alrb_workdir/rpms
cd $alrb_workdir/rpms

which yumdownloader > /dev/null 2>&1
if [ \$? -eq 0 ]; then
    yumdownloader --resolve $alrb_devRepo $alrb_rpms
else
    dnf download --resolve $alrb_devRepo $alrb_rpms
fi

cd $alrb_srcHome

ls -1 $alrb_workdir/rpms > ./os-rpms.txt
\cat /etc/os-release > ./buildmachineinfo.txt
\echo "uname=`uname -a`" >> ./buildmachineinfo.txt
\echo "HEP_OSlibs=`rpm -qa | \grep -e HEP_OSlibs`" >> ./buildmachineinfo.txt

for alrb_item in \`ls -1 $alrb_workdir/rpms/*.rpm\`; do
    rpm2cpio \$alrb_item | cpio -dim;
done
EOF

source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -c $alrb_osFlavor$alrb_osver -m $alrb_workdir/$alrb_repoName.repo:/etc/yum.repos.d/$alrb_repoName.repo -r "source /srv/payload.sh"
if [ $? -ne 0 ]; then
    exit 64
fi

cd $alrb_srcHome
\find $alrb_srcHome  -type d -exec chmod +w  {} \;
gtar zcf $alrb_workdir/$alrb_tag.tgz *
\echo "Ready: $alrb_workdir/$alrb_tag.tgz"

exit 0
