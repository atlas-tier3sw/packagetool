#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! make-xrootd.sh
#!
#! create a tarball for xrootd
#!
#! Usage:
#!     make-xrootd.sh  <version>
#!  should be run inside a container for the os/arch that you need.
#!
#! History:
#!    07Mar22: A. De Silva
#!
#!----------------------------------------------------------------------------

alrb_toolName="xrootd"

if [ ! -z $BASH_SOURCE ]; then
    alrb_packageToolHome=`\dirname ${BASH_SOURCE[0]}`
else
    alrb_packageToolHome=`\dirname $0`
fi
if [ "$alrb_packageToolHome" = "." ]; then
    alrb_packageToolHome=`pwd`
fi

if [ "$#" -ne 1 ]; then 
    \echo "Usage: make-$alrb_toolName.sh <version>"
    exit 64
fi
alrb_version="$1"

source $alrb_packageToolHome/setupEnvironment.sh $alrb_toolName $alrb_version
if [ $? -ne 0 ]; then
    exit 64
fi

cd $alrb_workdir

#alrb_durl="https://xrootd.slac.stanford.edu/download/v$alrb_version/xrootd-$alrb_version.tar.gz"
alrb_durl="https://xrootd.web.cern.ch/download/v$alrb_version/xrootd-$alrb_version.tar.gz"


mkdir -p $alrb_workdir/build
mkdir -p $alrb_workdir/source
mkdir -p $alrb_workdir/build/buildLogs
cd $alrb_workdir

curl -k -O  $alrb_durl
if [ $? -ne 0 ]; then
    exit 64
fi

alrb_tmpVal=`\find . -maxdepth 1 -name *.tar.gz`
cd $alrb_workdir/source
gtar zxf ../$alrb_tmpVal
cd $alrb_workdir/source

let alrb_xrootdVerN=`$ATLAS_LOCAL_ROOT_BASE/utilities/convertToDecimal.sh "$alrb_version" 3`
	 
for alrb_pythonVer in ${alrb_pythonAr[@]}; do

    alrb_pyALRBVersion="$alrb_osFlavor$alrb_osver-$alrb_pythonVer"
    alrb_pythonExecutable="python`\echo $alrb_pythonVer | \cut -f 1 -d '.'`"
    
    \echo "

                   * * *
  Building $alrb_tag for python $alrb_pythonVer
                   * * *
"
    (
	
	alrb_otherCmakeOpts=""

	if [ -z $ATLAS_LOCAL_ROOT_BASE ]; then
  	    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
	fi
	source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
	lsetup -f cmake "python $alrb_pyALRBVersion"
	
	alrb_pyExe=`type -P $alrb_pythonExecutable`
	if [ ! -z $ATLAS_LOCAL_PYTHON_VERSION ]; then
	    alrb_pyIncDir=`\echo $alrb_pyExe | \sed -e 's|/bin/.*|/include|g'`
	    if [ -d $alrb_pyIncDir ]; then
   		alrb_otherCmakeOpts="$alrb_otherCmakeOpts -DPYTHON_INCLUDE_DIR=$alrb_pyIncDir "
	    fi
	fi

	alrb_pyVersion=`$alrb_pyExe -V 2>&1 | \awk '{print $2}'`
	let alrb_pyVersionN=`$ATLAS_LOCAL_ROOT_BASE/utilities/convertToDecimal.sh "$alrb_pyVersion" 3`
	# PEP517 issue for python 3.6 and older with xrootd 5.6.1 and newer ...
	if [[ $alrb_xrootdVerN -ge 50601 ]] && [[ $alrb_pyVersionN -le 30699 ]] ; then
	    alrb_otherCmakeOpts="$alrb_otherCmakeOpts -DPIP_OPTIONS=\"--verbose\""
	fi
	
	\rm -f $alrb_workdir/source/CMakeCache.txt
	alrb_cmd="cmake $alrb_workdir/source/xrootd-$alrb_version -DCMAKE_INSTALL_PREFIX=$alrb_workdir/build -DENABLE_PERL=FALSE -DCMAKE_BUILD_TYPE=RelWithDebInfo -DENABLE_PYTHON=TRUE -DPYTHON_EXECUTABLE=$alrb_pyExe $alrb_otherCmakeOpts"
	echo "Executing $alrb_cmd"
	$alrb_cmd 2>&1 | tee $alrb_workdir/build/buildLogs/cmake-$alrb_pythonVer.log
	if [ $? -ne 0 ]; then
	    exit 64
	fi
	
	make 2>&1 | tee $alrb_workdir/build/buildLogs/make-$alrb_pythonVer.log
	if [ $? -ne 0 ]; then
	    exit 64
	fi
	make install 2>&1 | tee $alrb_workdir/build/buildLogs/make-install-$alrb_pythonVer.log
	if [ $? -ne 0 ]; then
	    exit 64
	fi
	
    )    
done

\mkdir -p $alrb_workdir/build/pythonSupport
cd $alrb_workdir/build/pythonSupport
for alrb_item in  `find ../lib* -name site-packages | sed -e 's|/site-packages||g'`; do ln -s  $alrb_item; done
    
cd $alrb_workdir/build
gtar zcf ../$alrb_tag.tgz *
\echo "Ready: $alrb_workdir/$alrb_tag.tgz"

exit 0
