#!----------------------------------------------------------------------------
#!
#! setupEnvironment.sh
#!
#! defines the environment
#!
#! Usage:
#!     source setupEnvironment.sh <tool> <version> 
#!
#! History:
#!    22Sep23: A. De Silva
#!
#!----------------------------------------------------------------------------

alrb_thisTool="$1"
alrb_thisVersion="$2"
let alrb_commonRC=0

if [ -e /etc/os-release ]; then
    source /etc/os-release
    alrb_osver="`\echo $VERSION | \cut -f 1 -d ' ' | cut -f 1 -d '.'`"
else  
    alrb_osver=`lsb_release  -r | \awk '{print $2}' | \cut -d "." -f 1`
fi

if [ $alrb_osver -ge 9 ]; then
    alrb_osFlavor="el"
else
    alrb_osFlavor="centos"
fi

alrb_unameM=`uname -m`

alrb_pythonAr=()
if [ "$alrb_unameM" = "x86_64" ]; then
    if [ "$alrb_osver" = "7" ]; then
	alrb_pythonAr=( 
	    "3.9"
	)
    elif [ "$alrb_osver" = "8" ]; then
	 alrb_pythonAr=(
	     "3.9"
	     "3.11"
	     "3.12"
	 )
    elif [ "$alrb_osver" = "9" ]; then
	alrb_pythonAr=(
	    "3.9"
	    "3.11"
	    "3.12"
	)
    else
	\echo "Error: $alrb_osFlavor$alrb_osver not configured"
	let alrb_commonRC=64
    fi
elif [ "$alrb_unameM" = "aarch64" ]; then
    if [ "$alrb_osver" = "7" ]; then
	\echo "Error: $alrb_osFlavor$alrb_osver not supported for $alrb_unameM"
	let alrb_commonRC=64
    elif [ "$alrb_osver" = "8" ]; then
	 alrb_pythonAr=(
	     "3.9"
	     "3.11"
	     "3.12"
	 )
    elif [ "$alrb_osver" = "9" ]; then
        alrb_pythonAr=(
	    "3.9"
	    "3.11"
	    "3.12"
	)
    else
	\echo "Error: $alrb_osFlavor$alrb_osver not configured"
	let alrb_commonRC=64
    fi
else
    \echo "Error: uname -m ($alrb_unameM) does not return supported arch types"
    let alrb_commonRC=64
fi

alrb_tag="$alrb_thisVersion-$alrb_unameM-$alrb_osFlavor$alrb_osver"

if [ -z $TMPDIR ]; then
    \echo "TMPDIR not defined.  Will use PWD=$PWD"
    alrb_workdir=$PWD/$alrb_thisTool/$alrb_tag
else
    alrb_workdir=$TMPDIR/$alrb_thisTool/$alrb_tag
fi
\rm -rf $alrb_workdir
mkdir -p $alrb_workdir

unset alrb_thisTool alrb_thisVersion

return $alrb_commonRC
	 
