#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! templateBuild.sh
#!
#! This is a template example that will do whatever you want running inside an
#!  appropriate container
#!
#! Usage:
#!     templateBuild.sh <job-name> <cmake version>
#!  This should NOT be run inside a container; it will be run inside
#!   a container matching the OS version in the job-name arg
#!
#! History:
#!    11Sep23: A. De Silva
#!
#!----------------------------------------------------------------------------

alrb_toolName="templateBuild"

if [ ! -z $BASH_SOURCE ]; then
    alrb_packageToolHome=`\dirname ${BASH_SOURCE[0]}`
else
    alrb_packageToolHome=`\dirname $0`
fi
if [ "$alrb_packageToolHome" = "." ]; then
    alrb_packageToolHome=`pwd`
fi

if [ "$#" -ne 2 ]; then 
    \echo "Usage: $alrb_toolName.sh <job-name> <cmake version>" 
    \echo "       where <job-name> is, for example, 21.0--container_AthSimulation_x86_64-centos7-gcc62-opt"
    \echo "             <cmake version> is, for example, 3.11.0"
    exit 64
else
    JOB_NAME="$1"
    USE_CMAKE_VERSION="$2"
fi

# if pwd is /eos, we cannot start apptainer on it because of a bug; exit.
# https://github.com/apptainer/apptainer/issues/1217
pwd | \grep -e "^/eos" > /dev/null 2>&1
if [ $? -eq 0 ]; then
    \echo "Error: Current dir is `pwd`; we cannot start a container on /eos."
    exit 64
fi

eval "$( echo ${JOB_NAME} | sed 's,^\([^_]\+\)_\([^_]\+\)_\([^\-]\+\)-\([^\-]\+\)-\([^\-]\+\)-\(.*\)$,branch_and_suffix=\1\;project=\2\; arch=\3\;flavour=\4\ comp=\5\; mode=\6\;,' )"

alrb_hostArch="`uname -m`"
if [ "$arch" != "$alrb_hostArch" ]; then
    \echo "Error: This machine is $alrb_hostArch; it cannot run $arch containers"
    exit 64
fi

# create the script that will run inside the container (./myPayload.sh)
myPayload="./myPayload-$JOB_NAME.sh"
if [ -e $myPayload ]; then
    \echo "Error: $myPayload exists.  Please delete it."
    exit 64
fi
\cat <<EOF > $myPayload
# simple way to just define the env from the host into the payload ...
ATLAS_JENKINS_TIMESTAMP=$ATLAS_JENKINS_TIMESTAMP
JOB_NAME=$JOB_NAME
branch_and_suffix=$branch_and_suffix
project=$project
arch=$arch
flavour=$flavour
comp=$comp
mode=$mode
USE_CMAKE_VERSION=$USE_CMAKE_VERSION

datestamp_init=\`date +%Y-%m-%dT%H%M -d"\${ATLAS_JENKINS_TIMESTAMP}"\`
[[ "\${datestamp_init}" = "" ]] && datestamp_init=\`date +%Y-%m-%dT%H%M\`
echo "Starting \$JOB_NAME build \$datestamp_init on" \`hostname\` "\${ATLAS_JENKINS_TIMESTAMP}"

# just do whatever needs to be done now ...
# use a backslash to preserve an env substitution from host
#  (eg \$XYZ as shown below)

ls -ltr /cvmfs/sft.cern.ch/lcg/releases/gcc/

\echo "asetup none,\$comp,\$mode --cmakesetup --cmakeversion \$USE_CMAKE_VERSION"
asetup none,\$comp,\$mode --cmakesetup --cmakeversion \$USE_CMAKE_VERSION
if [ \$? -ne 0 ]; then
   \echo "Error: asetuop failed"
   exit 64
fi 

alias

which python; python -V
which python3; python3 -V
which cmake; cmake --version

# etc

EOF

# run the payload in the appropriate container and return the correct exit code
if [ -z "${ATLAS_LOCAL_ROOT_BASE}" ]; then
    export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
fi
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c $flavour -r "source $myPayload"

exit $?
